# ELE410 Session4 : BLE example
___
## Introduction

This project is a port of ST's Sensor Demo example project provided with X-Cube-BLE package. This project is just an adaptation of this example, modified to run with the environment we use in this ELE410 course (Eclipse / GNU ARM plugins / GNU ARM GCC Toolchain / GDB + OpenOCD debugging).
___
## Pre-requisites 

Having a proper development environment (cf. ELE410 Template Project)
___
## Sample project description (extract from ST's readme.txt)
Main function to show how to use the BlueNRG Bluetooth Low Energy expansion board to send data from an STM32 Nucleo board to a smartphone with the support BLE and the "BlueNRG" app freely available on both GooglePlay and iTunes.

The URL to the iTunes for BlueNRG is [http://itunes.apple.com/app/bluenrg/id705873549?uo=5](http://itunes.apple.com/app/bluenrg/id705873549?uo=5)
The URL to the GooglePlay is [https://play.google.com/store/apps/details?id=com.st.blunrg](https://play.google.com/store/apps/details?id=com.st.blunrg)

The NUCLEO board will act as Server-Peripheral.
After establishing the connection:
- by pressing the USER button on the board, the cube showed by the app on the smartphone in the MOTION tab will rotate on the
  three axes (x,y,z).
- in the ENVIRONMENT tab of the app the temperature, pressure and humidity fake values sent by the STM32 Nucleo to the smartphone are shown.
- in the OTHER tab of the app, the RSSI value is shown.
For testing this sample application, a smartphone (acting as Central device) with the "BlueNRG" app running is needed.

This sample project shows also how to add new GATT services and characteristics.
Setting the NEW_SERVICES define to 1 (in \Main\sensor_service.h),
two new services are enabled:
  - the Time Server service, which has two characteristics
      - seconds, a read only characteristic which exposes the number of seconds
        passed since system boot
      - minutes, a characteristic which exposes the number of minutes passed
        since system boot. This characteristic can be read by GATT server, and a
        “notify” event is generated for this characteristic at one minute intervals.
  - the LED service, which can be used to control the state of LED2 present on the
    STM32 Nucleo board. This service has a writable “LED button characteristic”,
    which controls the state of LED2.
    When the GATT client application modifies the value of this characteristic, LED2
    is toggled.
    
Hence the application advertises its services and characteristics to the listening client
devices while waiting for a connection to be made.
After the connection is created by the central device, data is periodically updated. 
